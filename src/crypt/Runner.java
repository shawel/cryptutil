package crypt;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Created by shawelnegussie on 10/26/15.
 */
public class Runner {

    public static String readFileText(String filename) throws IOException {
        FileReader r = new FileReader(filename);
        BufferedReader reader= new BufferedReader(r);
        String pkey = "";
        String line = "";
        while((line = reader.readLine())!= null) {
            pkey = pkey+line;

        }
        return  pkey;
    }

    public static byte[] readFileBytes(String filename) throws IOException
    {
        Path path = Paths.get(filename);
        return Files.readAllBytes(path);
    }

    public static void main(String[] args) {

        try {

            String privateKey = readFileText("private_key_pcks8.pem");
            String publicKey = readFileText("public_key.pem");


            CryptUtil cryptUtil = new CryptUtil(publicKey,privateKey, CryptUtil.KeyType.PEM);
            byte [] encryptedMessage = cryptUtil.encrypt("THIS IS A CRYPTED MESSAGE USING PEM");
            System.out.println(encryptedMessage);
            String decryptedMessage = cryptUtil.decrypt(encryptedMessage);
            System.out.println(decryptedMessage);



            byte [] bprivateKey = readFileBytes("private_key_pcks8.der");
            byte [] bpublicKey = readFileBytes("public_key.der");

            CryptUtil cryptUtilDER = new CryptUtil(bpublicKey,bprivateKey, CryptUtil.KeyType.DER);
            encryptedMessage = cryptUtilDER.encrypt("THIS IS A CRYPTED MESSAGE USING DER");
            System.out.println(encryptedMessage);
            decryptedMessage = cryptUtilDER.decrypt(encryptedMessage);
            System.out.println(decryptedMessage);

        } catch (IOException e) {
            e.printStackTrace();

        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }


    }

}

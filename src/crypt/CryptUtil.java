package crypt;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by shawelnegussie on 10/24/15.
 */
public class CryptUtil {

    public  static enum KeyType {
        PEM,
        DER
    };

    String privateKey;
    String publicKey;
    byte[] bprivateKey;
    byte[] bpublicKey;
    PublicKey oPublicKey;
    PrivateKey oPrivateKey;
    KeyType keyType;

    private CryptUtil(){

    }

    public CryptUtil(String publicKey, String privateKey, KeyType kType) throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
        this.keyType = kType;
        loadKeys();

    }

    public CryptUtil(byte[] publicKey, byte[] privateKey, KeyType kType) throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException {
        this.bprivateKey = privateKey;
        this.bpublicKey = publicKey;
        this.keyType = kType;
        loadKeys();

    }

    private void  loadKeys() throws InvalidKeySpecException, NoSuchAlgorithmException, UnsupportedEncodingException {
        if( this.keyType == KeyType.DER) {
            this.oPrivateKey = loadDERPrivateKey();
            this.oPublicKey = loadDERPublicKey();
        } else if(this.keyType == KeyType.PEM){
            this.oPrivateKey = loadPEMPrivateKey();
            this.oPublicKey = loadPEMPublicKey();
        }
    }

    private PrivateKey loadDERPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {

        byte [] encodedKey = bprivateKey;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePrivate(keySpec);

    }

    private PrivateKey loadPEMPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        privateKey = privateKey.replace("-----BEGIN PRIVATE KEY-----","").replace("-----END PRIVATE KEY-----","");
        byte [] encodedKey = Base64.decode(privateKey);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;

    }

    private PublicKey loadDERPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {
        byte [] encodedKey = bpublicKey;
        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(encodedKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(publicSpec);

    }

    private PublicKey loadPEMPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        publicKey = publicKey.replace("-----BEGIN PUBLIC KEY-----","").replace("-----END PUBLIC KEY-----","");

        byte [] encoded = Base64.decode(publicKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(encoded);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey publicKey =kf.generatePublic(publicSpec);
        return publicKey;

    }

    public byte[] encrypt(String plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        byte[]  bplaintext = plaintext.getBytes("UTF8");
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, oPublicKey );
        return cipher.doFinal(bplaintext); // new String(bcipherText, "UTF8");
    }

    public String decrypt(byte [] ciphertext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.DECRYPT_MODE, oPrivateKey);
        byte [] bplainText = cipher.doFinal(ciphertext);
        return  new String(bplainText, "UTF8");
    }

    public byte[] encrypt(PublicKey key, byte[] plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(plaintext);
    }

    public byte[] decrypt(PrivateKey key, byte[] ciphertext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(ciphertext);
    }
}

# openssl public private key generator make file


passwd = 'WRI'
pfxfile = 'DriverPII'
prkey = 'private_key'
cert = 'certificate'
pubkey = 'public_key'
servercert = 'server_certificate'

message = 'This is a Cipher Text'

# test encrypting with public key and decrypt using private key
test: der
	echo $(message) > message.txt
	openssl rsautl -encrypt -inkey $(pubkey).pem -pubin -in message.txt -out encrypt.data
	openssl rsautl -decrypt -inkey $(prkey).pem  -in encrypt.data -out dmessage.txt
	cat dmessage.txt


# der formatted - easier for java to comsunme since it is in binary format
der: pcks8
	openssl pkcs8 -topk8  -inform PEM -outform DER -in $(prkey).pem -out $(prkey)_pcks8.der -nocrypt
	openssl rsa -in $(prkey).pem -pubout -outform der  -out $(pubkey).der



# pcks8 version for java consumable private key
pcks8: pfx
	openssl pkcs8 -topk8 -nocrypt -in $(prkey).pem -out $(prkey)_pcks8.pem



# create public private key and certificate and server certificate from pks file
pfx:	
	openssl pkcs12 -in $(pfxfile).pfx -nocerts -out $(prkey).pem -nodes -password pass:$(passwd)
	openssl pkcs12 -in $(pfxfile).pfx -nokeys -out $(cert).pem -password pass:$(passwd)
	openssl x509 -pubkey -in $(cert).pem -noout > $(pubkey).pem
	openssl rsa -in $(prkey).pem -out $(servercert).cert

clean:
	-rm $(prkey).pem
	-rm $(cert).pem
	-rm $(pubkey).pem
	-rm $(servercert).cert
	-rm $(prkey)_pcks8.pem
	-rm $(prkey)_pcks8.der
	-rm $(pubkey).der
	-rm message.txt
	-rm encrypt.data
	-rm dmessage.txt

